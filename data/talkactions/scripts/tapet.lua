function onUse(cid, item, frompos, item2, topos)

Itempos = {x=topos.x,y=topos.y,z=topos.z,stackpos=1}
Item = getThingfromPos(Itempos)
access = 6

if getPlayerAccess(cid) >= access then
if Item.itemid > 0 then
doSendMagicEffect(topos, 70)
doRemoveItem(Item.uid, 1)
doSendAnimatedText(Itempos,"Removed.", TEXTCOLOR_RED)

end
end
return 1
end