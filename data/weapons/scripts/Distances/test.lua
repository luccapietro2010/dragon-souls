local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat1, COMBAT_PARAM_DISTANCEEFFECT, 42)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,4)
level = getPlayerLevel(cid)
min = -((skill*14)+level*4+maglevel*1)
max = -((skill*15)+level*5+maglevel*1)
return min, max
end

setCombatCallback(combat1, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat2, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_ETHEREALSPEAR)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,4)
min = -((skill*26)+maglevel*3+level*7)
max = -((skill*29)+maglevel*3+level*8)
return min, max
end

setCombatCallback(combat2, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local condition = createConditionObject(CONDITION_PARALYZE)
setConditionParam(condition, CONDITION_PARAM_TICKS, 1000)
setConditionFormula(condition, -0.7, 0, -0.7, 0)


local frozt = createConditionObject(CONDITION_FROZZEN)
setConditionParam(frozt, CONDITION_PARAM_TICKS, 1000)

function onUseWeapon(cid, var)

    local function frost(target)
     doSendAnimatedText(getThingPos(target),"Freeze!",215)
          end

     local target = getCreatureTarget(cid)
     local freeze = 
   { lookType = getCreatureOutfit(target).lookType,
     lookHead = 9,
     lookBody = 9,
     lookLegs = 9, 
     lookFeet = 9, 
     lookAddons = getCreatureOutfit(target).lookAddons }
	fala = math.random(10,10)
	rand = math.random(1,1000)
     if(target ~= 0) then
	if rand <= getPlayerSkill(cid,4) then
	if fala == 10 then
 	doPlayerSay(cid,"Its cool!",16)
     doSetCreatureOutfit(target, freeze, 2000)
          addEvent(frost, 1*500,target)
     doTargetCombatCondition(0, target, frozt, CONST_ME_NONE)
	doPlayerAddHealth(cid,(getPlayerMaxHealth(cid)/10))
     doPlayerAddMana(cid,(getPlayerMaxMana(cid)/10))
 	doSendAnimatedText(getPlayerPosition(cid),"Critical!",129)
     doTargetCombatCondition(0, target, condition, CONST_ME_NONE)
    	doCombat(cid, combat2, var)
else
	doPlayerAddHealth(cid,(getPlayerMaxHealth(cid)/10))
 	doSendAnimatedText(getPlayerPosition(cid),"Critical!",129)
    	doCombat(cid, combat2, var)
end
else
    	doCombat(cid, combat1, var)
end
end
end