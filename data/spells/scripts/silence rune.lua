local combat = createCombatObject()
setCombatParam(combat, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat, COMBAT_PARAM_EFFECT, 32)
setCombatFormula(combat, COMBAT_FORMULA_LEVELMAGIC, -13.7, 0, -19.9, 0)

local condition = createConditionObject(CONDITION_PARALYZE)
setConditionParam(condition, CONDITION_PARAM_TICKS, 6000)
setConditionParam(condition, CONDITION_PARAM_SPEED, -220)
setConditionFormula(condition, -0.8, 0, -0.8, 0)
setCombatCondition(combat, condition)

local exhaust = createConditionObject(CONDITION_EXHAUSTED)
setConditionParam(exhaust, CONDITION_PARAM_TICKS, 6000)

local arr = {
{0, 1, 1, 1, 0},
{1, 1, 1, 1, 1},
{1, 1, 3, 1, 1},
{1, 1, 1, 1, 1},
{0, 1, 1, 1, 0}
}

local area = createCombatArea(arr)
setCombatArea(combat, area)


function onTargetCreature(cid, target)

  
 if isPlayer(target) == 1 then
   rand = math.random(1,5)
   if rand == 1 then
     doSendAnimatedText(getThingPos(cid),"Silence!",215)
   doTargetCombatCondition(0, cid, exhaust, CONST_ME_NONE)
  setPlayerStorageValue(cid, exhausted_storagevalue, os.time() + exhausted_seconds)
 setPlayerStorageValue(cid, exhausted_storagevalue1, os.time() + exhausted_seconds1)
   else
     doSendAnimatedText(getThingPos(target),"Silence!",215)
   doTargetCombatCondition(0, target, exhaust, CONST_ME_NONE)
  setPlayerStorageValue(target, exhausted_storagevalue, os.time() + exhausted_seconds)
 setPlayerStorageValue(target, exhausted_storagevalue1, os.time() + exhausted_seconds1)
   end
   end
 end

setCombatCallback(combat, CALLBACK_PARAM_TARGETCREATURE, "onTargetCreature")

local function Cooldown(cid)
if isPlayer(cid) == TRUE then
doPlayerSendTextMessage(cid,MESSAGE_STATUS_WARNING,'CD: Exevo Gran Mas Time Ultime')
end
end

local exhausted_seconds = 35 -- Segundos que o Player Poder� castar a spell novamente
local exhausted_storagevalue = 9261 -- Storage Value do Cool Down

function onCastSpell(cid, var)
if(os.time() < getPlayerStorageValue(cid, exhausted_storagevalue)) then
doPlayerSendCancel(cid,'O Cooldown n�o est� pronto.')
return TRUE
end
rand = math.random(1,50)
	if rand == 1 and isPlayer(cid) == 1 then
 	doPlayerSay(cid,"Stop Time!",16)
      addEvent(Cooldown, 1*55000,cid)
         setPlayerStorageValue(cid, exhausted_storagevalue, os.time() + exhausted_seconds)
	return doCombat(cid, combat, var)
	elseif rand == 2 and isPlayer(cid) == 1 then
 	doPlayerSay(cid,"Stop Bitch!",16)
      addEvent(Cooldown, 1*55000,cid)
         setPlayerStorageValue(cid, exhausted_storagevalue, os.time() + exhausted_seconds)
	return doCombat(cid, combat, var)
else
      addEvent(Cooldown, 1*55000,cid)
         setPlayerStorageValue(cid, exhausted_storagevalue, os.time() + exhausted_seconds)
	return doCombat(cid, combat, var)
end
end