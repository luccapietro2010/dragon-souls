local condition = createConditionObject(CONDITION_ENERGY)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 0, 0, 0)

function onUse(cid, item, frompos, item2, topos)
skill = getPlayerSkill(cid,0)
maglevel = getPlayerMagLevel(cid)
level = getPlayerLevel(cid)
min = ((level*200)+skill+maglevel)
max = ((level*1000)+skill+maglevel)

exp = math.random(min,max)

if getPlayerLevel(cid) <= 149 then
doPlayerSendTextMessage(cid,20,'Somente jogadores com Level superior � 150 podem usar este Elixir of Experience.')
elseif getPlayerSoul(cid) >= 250 then

              PlayerLevel = getPlayerLevel(cid)

if PlayerLevel < 510 then
doPlayerAddExp(cid,exp)
doTargetCombatCondition(0, cid, condition, CONST_ME_MAGIC_RED)
doPlayerSendTextMessage(cid,20,'Voce recebeu ' .. exp .. ' de experi�ncia.')
doSendAnimatedText(getPlayerPosition(cid),exp, 179)
doPlayerAddSoul(cid,-250)
doRemoveItem(item.uid,1)
end
else
doPlayerSendTextMessage(cid,20,'Voce n�o tem Souls suficiente.')
end
end